# Introduction
This project aims to provide a way to count the lines of code in a project folder and display that information to the user. In addition exporting the data is in the works.
# Usage
## First Setup
To use `linestat` just copy it into your `bin` folder on your Unix system. After that, you have the option of just simply running the program and it will list all the lines of files in the current directory aswell as subdirectories.
## General
If you want to know the line counts of a specific file or subfolder just name the relative filepath as an argument like this:
```linestat [path]```
The amount of arguments you can provide is not limited.
## Output
The program outputs content formated like this:
```
+-Name-+-Lines-+-Lines NBL-+-Blank Lines-+
| ...  |  ...  |    ...    |     ...     |
+------+-------+-----------+-------------+
| sum  |  ...  |    ...    |     ...     |
+------+-------+-----------+-------------+
```
1.  `name` refers to the file name
2.  `lines` refers to the actual amount of lines in the file
3.  `lines nbl` refers to the lines without the blank lines
4.  `blank lines` refers to the amount of blank lines in the file
5.  `sum` refers to the row of all the data summed up.
## Exporting the output
Currently the program supports exporting the data as a CSV file using the `--export-csv` flag and specifying the path of the output file:
```
linestat --export-csv [outputFilePath] [other]
```
# Examples
## 1. Counting the lines of code in the `src` folder
Running the command `linestat` in the repositories source folder yields the following result:
```
+------Name------+-Lines-+-Lines NBL-+-Blank Lines-+
| src/main.cpp   |    68 |        61 |           7 |
| src/line.cpp   |   160 |       138 |          22 |
| src/lineIO.cpp |    37 |        33 |           4 |
+----------------+-------+-----------+-------------+
| sum            |   265 |       232 |          33 |
+----------------+-------+-----------+-------------+
```
## 2. Counting the lines of code in this repository
This will be an example of how to use this application to count the lines of code of a project.
Navigate to the root directory of your project and simply give the subfolders containing your code as arguments.
For example for this project it would be this:
```
linestat ./src/ ./include/
```
or alternativly
```
linestat src include
```
which will lead to the output of:
```
+------------------name------------------+-------lines-------+-----lines nbl-----+----blank lines----+
| src/main.cpp                           |                46 |                41 |                 5 |
| src/line.cpp                           |               153 |               136 |                17 |
| src/lineIO.cpp                         |                 6 |                 5 |                 1 |
| include/line.h                         |                19 |                17 |                 2 |
| include/lineIO.h                       |                10 |                 7 |                 3 |
| include/defines.h                      |                15 |                12 |                 3 |
+----------------------------------------+-------------------+-------------------+-------------------+
| sum                                    |               249 |               218 |                31 |
+----------------------------------------+-------------------+-------------------+-------------------+
```
