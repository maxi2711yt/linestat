#!/bin/bash

x86_64-w64-mingw32-g++ -std=c++17 --static -Wall -D_WIN32 -Wextra -pedantic -o linestat.exe ./src/main.cpp ./src/line.cpp ./src/lineIO.cpp
