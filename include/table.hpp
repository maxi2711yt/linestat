// v1.0
//
// Copyright (c) 2022 Maximilian Barsch (maximilian.barsch@outlook.de)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once

#include <cstdio>
#include <filesystem>
#include <ios>
#include <iostream>
#include <locale>
#include <stddef.h>
#include <stdexcept>
#include <type_traits>
#include <fstream>

#include <locale>
#include <codecvt>

#ifdef WCHAR_TABLE
    typedef wchar_t character;
    typedef std::wstring string;
#define PRINT(x) std::wcout << x
#define PRINTLN(x) std::wcout << x << std::endl
#else
    typedef std::string string;
    typedef char character;
#define PRINT(x) std::cout << x
#define PRINTLN(x) std::cout << x << std::endl
#endif

#define DEFAULT_COLUMN_WIDTH 10
#define DEFAULT_PADDING 1

#ifdef WCHAR_TABLE
#define DEFAULT_HORIZONTAL_BORDER_CHAR (wchar_t)'-'
#define DEFAULT_VERTICAL_BORDER_CHAR (wchar_t)'|'
#define DEFAULT_CORNER_CHAR (wchar_t)'+'
#define DEFAULT_CSV_DIV_CHAR (wchar_t)';'
#define SPACE_CHAR (wchar_t)' '
#define EMPTY_STRING L""
#else
#define DEFAULT_HORIZONTAL_BORDER_CHAR '-'
#define DEFAULT_VERTICAL_BORDER_CHAR '|'
#define DEFAULT_CORNER_CHAR '+'
#define DEFAULT_CSV_DIV_CHAR ';'
#define SPACE_CHAR ' '
#define EMPTY_STRING ""
#endif

typedef struct CORNER
{
    bool a;
    bool b;
    bool c;
    bool d;
} corner_s;

namespace types
{

enum class eAlignment : int
{
    ALIGN_LEFT = 0,
    ALIGN_CENTER = 1,
    ALIGN_RIGHT = 2
};

template <typename T> [[nodiscard]] inline string to_string(const T value)
{
#ifdef WCHAR_TABLE
    return std::to_wstring(value);
#else 
    return std::to_string(value);
#endif
}

template <> [[nodiscard]] inline string to_string(const string value)
{
    return value;
}

#ifdef WCHAR_TABLE
template<> [[nodiscard]] inline string to_string(const std::string value)
{
    return std::wstring_convert<std::codecvt_utf8<wchar_t>>().from_bytes(value);
}
#endif

template <> [[nodiscard]] inline string to_string(const character* value)
{
    return string(value);
}

template <typename T> class Cell
{
  public:
    Cell();
    Cell(T value);
    Cell(T value, const bool upper_border, const bool lower_border, const bool left_border, const bool right_border);
    ~Cell();

    [[nodiscard]] const T &data() const noexcept; // Returns the Data stored in the Cell as a const reference.
    void data(const T data) noexcept;             // Sets the data for the cell.

    [[nodiscard]] bool getDrawStateOfUpperBorder() const noexcept; // Returns the draw state of the upper border
    [[nodiscard]] bool getDrawStateOfLowerBorder() const noexcept; // Returns the draw state of the lower border
    [[nodiscard]] bool getDrawStateOfLeftBorder() const noexcept;  // Returns the draw state of the left border
    [[nodiscard]] bool getDrawStateOfRightBorder() const noexcept; // Returns the draw state of the right border

    void setDrawStateOfUpperBorder(const bool value) noexcept; // Sets the draw state of the upper border.
    void setDrawStateOfLowerBorder(const bool value) noexcept; // Sets the draw state of the lower border.
    void setDrawStateOfLeftBorder(const bool value) noexcept;  // Sets the draw state of the left border.
    void setDrawStateOfRightBorder(const bool value) noexcept; // Sets the draw state of the right border.

    [[nodiscard]] bool getStateOfUpperBorder() const noexcept; // Returns the draw of the upper border
    [[nodiscard]] bool getStateOfLowerBorder() const noexcept; // Returns the draw of the lower border
    [[nodiscard]] bool getStateOfLeftBorder() const noexcept;  // Returns the draw of the left border
    [[nodiscard]] bool getStateOfRightBorder() const noexcept; // Returns the draw of the right border

    void setStateOfUpperBorder(const bool value) noexcept; // Sets the draw of the upper border.
    void setStateOfLowerBorder(const bool value) noexcept; // Sets the draw of the lower border.
    void setStateOfLeftBorder(const bool value) noexcept;  // Sets the draw of the left border.
    void setStateOfRightBorder(const bool value) noexcept; // Sets the draw of the right border.

    [[nodiscard]] character getUpperBorderChar() const noexcept; // Returns the char for upper border.
    [[nodiscard]] character getLowerBorderChar() const noexcept; // Returns the char for lower border.
    [[nodiscard]] character getLeftBorderChar() const noexcept;  // Returns the char for left border.
    [[nodiscard]] character getRightBorderChar() const noexcept; // Returns the char for right border.

    [[nodiscard]] character getUpperLeftCornerChar() const noexcept;  // Returns the char for upper left corner.
    [[nodiscard]] character getUpperRightCornerChar() const noexcept; // Returns the char for upper right corner.
    [[nodiscard]] character getLowerLeftCornerChar() const noexcept;  // Returns the char for lower left corner.
    [[nodiscard]] character getLowerRightCornerChar() const noexcept; // Returns the char for lower right corner.

    void setUpperBorderChar(const character value) noexcept; // Sets the char for the upper border.
    void setLowerBorderChar(const character value) noexcept; // Sets the char for the lower border.
    void setLeftBorderChar(const character value) noexcept;  // Sets the char for the left border.
    void setRightBorderChar(const character value) noexcept; // Sets the char for the right border.

    void setUpperLeftCornerChar(const character value) noexcept;  // Sets the char for the upper left corner.
    void setUpperRightCornerChar(const character value) noexcept; // Sets the char for the upper right corner.
    void setLowerLeftCornerChar(const character value) noexcept;  // Sets the char for the lower left corner.
    void setLowerRightCornerChar(const character value) noexcept; // Sets the char for the lower right corner.

    void setDrawingOfBorders(const bool upper_border, const bool lower_border, const bool left_border,
                             const bool right_border) noexcept; // Sets the states of all borders.

    void setAlignment(const eAlignment alignment) noexcept; // Sets the alignment of the data in the cell.
    [[nodiscard]] eAlignment getAlignment() const noexcept; // Returns the alignment of the data in the cell.

    [[nodiscard]] string getHeading() const noexcept; // Gets the heading of the cell.
    void setHeading(const string heading) noexcept;   // Sets the heading of the cell.
  private:
    T _data;              // Stores the data of a cell.
    string _heading; // Stores the cells heading.

    bool _upper_border; // Stores the state of the upper border.
    bool _lower_border; // Stores the state of the lower border.
    bool _left_border;  // Stores the state of the left corner.
    bool _right_border; // Stores the state of the left corner.

    bool _draw_upper_border; // Stores the state of the upper border.
    bool _draw_lower_border; // Stores the state of the lower border.
    bool _draw_left_border;  // Stores the state of the left corner.
    bool _draw_right_border; // Stores the state of the left corner.

    character _upper_border_char;       // Stores the char of the upper border.
    character _lower_border_char;       // Stores the char of the lower border.
    character _left_border_char;        // Stores the char of the left corner.
    character _right_border_char;       // Stores the char of the left corner.
    character _upper_left_corner_char;  // Stores the char for the upper left corner of the cell.
    character _upper_right_corner_char; // Stores the char for the upper right corner of the cell.
    character _lower_left_corner_char;  // Stores the char for the lower left corner of the cell.
    character _lower_right_corner_char; // Stores the char for the lower right corner of the cell.
    eAlignment _alignment;         // Stores the current alignment.

    inline void setCellBordersTrue() noexcept;
};

template <typename T> class Table
{
  public:
    Table(const size_t rows, const size_t columns);
    ~Table();
    
    [[nodiscard]] inline T to_type(const string value) const noexcept;
#ifdef WCHAR_TABLE
    [[nodiscard]] inline T to_type(const std::string value) const noexcept;
#endif

    [[nodiscard]] size_t rows() const noexcept;    // Retuns the number of rows of the table.
    [[nodiscard]] size_t columns() const noexcept; // Returns the number of columns of the table.

    [[nodiscard]] size_t padding() const noexcept; // Return the currently defined padding.
    void padding(const size_t value) noexcept;     // Sets the padding for the cells.

    void addRow();    // Adds a row to the table.
    void addColumn(); // Adds a column to the table.

    void removeColumn(const size_t column); // Removes the column at the specified index. Throws std::out_of_range exception if col is out of bounds.
    void removeRow(const size_t row); // Removes the row at the specified index. Thros std::out_of_range exception if row is out of bounds.
    void insertCol(const size_t column); // Inserts a column at the specified index. Throws std::out_of_range exception if col is out of bounds.
    void insertRow(const size_t row); // Inserts a row at the specified index. Throws std::out_of_range exception if row is out of bounds.

    [[nodiscard]] const Cell<T> &getCell(
        const size_t row, const size_t column) const; // Retuns a const reference to a specific cell. Throws
                                                      // std::out_of_range exception if row or column are out of bounds.
    void setCell(const size_t row, const size_t column,
                 const Cell<T> cell); // Sets a cells data with the provided cell. Throws std::out_of_range exception if
                                      // row or column are out of bounds.

    [[nodiscard]] const T &getElement(
        const size_t row, const size_t column) const; // Returns the data stored in the cell at (row, column). Throws
                                                      // std::out_of_range exception if row or column are out of bounds.
    void setElement(const size_t row, const size_t column,
                    const T value); // Sets the data for the cell at (row, column). Throws std::out_of_range exception
                                    // if row or column are out of bounds.

    void setColumnWidth(const size_t column,
                        const size_t width); // Sets the width of the specified column. Throws std::out_of_range
                                             // exception if column is out of bounds.
    [[nodiscard]] size_t getColumnWidth(const size_t column)
        const; // Returns the column width for the specific column. Throws std::out_of_range if column is out of bounds.

    void drawTable() const noexcept; // Draws the table to the standard output.
    void exportTableAsCSV(const std::filesystem::path path, const char div)
        const; // Exports the table as a CSV file to the specified path. 
    void importTableFromCSV(
        const std::filesystem::path path, const char div); // Imports the contents of a csv file into the table.

    void automaticallySizeColumns() noexcept; // Automatically sizes the columns according to the "biggest", in terms of
                                              // string size, element in the column.

    void setColumnHeading(const size_t column,
                          const string title); // Sets the heading for a specified column. Throws std::out_of_range
                                                    // exception if column is out of bounds.
    string getColumnHeading(const size_t column) const; // Returns the heading for a specified column. Throws
                                                             // sd::out_of_range exception if column is out of bounds.

    void enableDrawingCellBorders() noexcept;                        // Enables the drawing of the cell borders.
    void disableDrawingCellBorders() noexcept;                       // Disables the drawing of the cell border.
    [[nodiscard]] bool getCellBorderDrawingState() const noexcept;   // Returns true if cell borders are activated.
    void setCellBorderDrawingState(const bool state) const noexcept; // Sets the cell borders to the state given.

    void enableDrawingTableBorders() noexcept;  // Enables the drawing of the outer table border lines.
    void disableDrawingTableBorders() noexcept; // Disables the drawing of the outer table border lines.
    [[nodiscard]] bool getTableBorderDrawingState() const noexcept; // Retuns true if the Table lines are activated.
    void setTableBorderDrawingState(const bool state) noexcept;     // Allows you to set the drawing of the horizontal
                                                                    // dividing lines. true=on. false=off.

    [[nodiscard]] eAlignment getAlignmentOfCell(const size_t row, const size_t column)
        const; // Retuns the current alignment present in the specified cell. Throws std::out_of_range exception if row
               // or column are out of bounds.
    void setAlignmentOfCell(
        const size_t row, const size_t column,
        const eAlignment alignment); // Sets the alignment for the specified cell. Throws std::out_of_range exception if
                                     // either row or column are out of range.

    void setDrawingUpperBorderOfCell(
        const size_t row, const size_t column,
        const bool state); // Sets the upper border of the specified cell to the provided state. Throws
                           // std::out_of_range exception if either row or column are out of bounds.
    void setDrawingLowerBorderOfCell(
        const size_t row, const size_t column,
        const bool state); // Sets the lower border of the specified cell to the provided state. Throws
                           // std::out_of_range exception if either row or column are out of bounds.
    void setDrawingLeftBorderOfCell(
        const size_t row, const size_t column,
        const bool state); // Sets the left border of the specified cell to the provided state. Throws std::out_of_range
                           // exception if either row or column are out of bounds.
    void setDrawingRightBorderOfCell(
        const size_t row, const size_t column,
        const bool state); // Sets the right border of the specified cell to the provided state. Throws
                           // std::out_of_range exception if either row or column are out of bounds.

    void setUpperBorderOfCell(
        const size_t row, const size_t column,
        const bool state); // Sets the upper border of the specified cell to the provided state. Throws
                           // std::out_of_range exception if either row or column are out of bounds.
    void setLowerBorderOfCell(
        const size_t row, const size_t column,
        const bool state); // Sets the lower border of the specified cell to the provided state. Throws
                           // std::out_of_range exception if either row or column are out of bounds.
    void setLeftBorderOfCell(
        const size_t row, const size_t column,
        const bool state); // Sets the left border of the specified cell to the provided state. Throws std::out_of_range
                           // exception if either row or column are out of bounds.
    void setRightBorderOfCell(
        const size_t row, const size_t column,
        const bool state); // Sets the right border of the specified cell to the provided state. Throws
                           // std::out_of_range exception if either row or column are out of bounds.

    [[nodiscard]] string getHeadingOfCell(const size_t row, const size_t column)
        const; // Gets the heading of the specified cell. Throws std::out_of_range exception if either row or column are
               // out of bounds.
    void setHeadingOfCell(const size_t row, const size_t column,
                          const string value); // Sets the heading of the specified cell. Throws std::out_of_range
                                                    // exception if either row or column are out of bounds.

    [[nodiscard]] string generateTableAsString(
        const bool newline) const noexcept; // Returns the table as a string with lines seperated by new lines.

    void setUpperBorderCharOfCell(const size_t row, const size_t column, const character c);
    void setLowerBorderCharOfCell(const size_t row, const size_t column, const character c);
    void setLeftBorderCharOfCell(const size_t row, const size_t column, const character c);
    void setRightBorderCharOfCell(const size_t row, const size_t column, const character c);

    void setUpperLeftCornerCharOfCell(const size_t row, const size_t column, const character c);
    void setUpperRightCornerCharOfCell(const size_t row, const size_t column, const character c);
    void setLowerLeftCornerCharOfCell(const size_t row, const size_t column, const character c);
    void setLowerRightCornerCharOfCell(const size_t row, const size_t column, const character c);

  private:
    void deleteData() noexcept;
    void deleteWidths() noexcept;

    [[nodiscard]] string getAlignedString(const string content, const size_t target_width,
                                               const types::eAlignment alignment, const character filler) const noexcept;
    size_t _rows;    // Stores the amount of rows in the table.
    size_t _columns; // Stores the amount of columns in the table.
    Cell<T> **_data; // Stores the cells.

    size_t *_column_widths; // Stores the widths of the cells.
    size_t _padding;        // Stores the padding of the cells

    bool _CellBorderDrawingState;
    bool _TableBordersDrawingState;
};
} // namespace types

template<> [[nodiscard]] inline string types::Table<string>::to_type(const string value) const noexcept
{
    return value;
}

template <typename T> [[nodiscard]] character types::Cell<T>::getUpperBorderChar() const noexcept
{
    return this->_upper_border_char;
}

template <typename T> [[nodiscard]] character types::Cell<T>::getLowerBorderChar() const noexcept
{
    return this->_lower_border_char;
}

template <typename T> [[nodiscard]] character types::Cell<T>::getLeftBorderChar() const noexcept
{
    return this->_left_border_char;
}

template <typename T> [[nodiscard]] character types::Cell<T>::getRightBorderChar() const noexcept
{
    return this->_right_border_char;
}

template <typename T> [[nodiscard]] character types::Cell<T>::getUpperLeftCornerChar() const noexcept
{
    return this->_upper_left_corner_char;
}

template <typename T> [[nodiscard]] character types::Cell<T>::getUpperRightCornerChar() const noexcept
{
    return this->_upper_right_corner_char;
}

template <typename T> [[nodiscard]] character types::Cell<T>::getLowerLeftCornerChar() const noexcept
{
    return this->_lower_left_corner_char;
}

template <typename T> [[nodiscard]] character types::Cell<T>::getLowerRightCornerChar() const noexcept
{
    return this->_lower_right_corner_char;
}

template <typename T> void types::Cell<T>::setUpperBorderChar(const character value) noexcept
{
    this->_upper_border_char = value;
}

template <typename T> void types::Cell<T>::setLowerBorderChar(const character value) noexcept
{
    this->_lower_border_char = value;
}

template <typename T> void types::Cell<T>::setLeftBorderChar(const character value) noexcept
{
    this->_left_border_char = value;
}

template <typename T> void types::Cell<T>::setRightBorderChar(const character value) noexcept
{
    this->_right_border_char = value;
}

template <typename T> void types::Cell<T>::setUpperLeftCornerChar(const character value) noexcept
{
    this->_upper_left_corner_char = value;
}

template <typename T> void types::Cell<T>::setUpperRightCornerChar(const character value) noexcept
{
    this->_upper_right_corner_char = value;
}

template <typename T> void types::Cell<T>::setLowerLeftCornerChar(const character value) noexcept
{
    this->_lower_left_corner_char = value;
}

template <typename T> void types::Cell<T>::setLowerRightCornerChar(const character value) noexcept
{
    this->_lower_right_corner_char = value;
}

template <typename T> void types::Cell<T>::setAlignment(const types::eAlignment alignment) noexcept
{
    this->_alignment = alignment;
}

template <typename T> types::eAlignment types::Cell<T>::getAlignment() const noexcept
{
    return this->_alignment;
}

template <typename T> inline void types::Cell<T>::setCellBordersTrue() noexcept
{
    this->_upper_border = true;
    this->_lower_border = true;
    this->_left_border = true;
    this->_right_border = true;
}

template <typename T>
void types::Cell<T>::setDrawingOfBorders(const bool upper_border, const bool lower_border, const bool left_border,
                                         const bool right_border) noexcept
{
    this->_draw_upper_border = upper_border;
    this->_draw_lower_border = lower_border;
    this->_draw_left_border = left_border;
    this->_draw_right_border = right_border;
}

template <typename T> [[nodiscard]] bool types::Cell<T>::getDrawStateOfUpperBorder() const noexcept
{
    return this->_draw_upper_border;
}

template <typename T> [[nodiscard]] bool types::Cell<T>::getDrawStateOfLowerBorder() const noexcept
{
    return this->_draw_lower_border;
}

template <typename T> [[nodiscard]] bool types::Cell<T>::getDrawStateOfLeftBorder() const noexcept
{
    return this->_draw_left_border;
}

template <typename T> [[nodiscard]] bool types::Cell<T>::getDrawStateOfRightBorder() const noexcept
{
    return this->_draw_right_border;
}

template <typename T>

void types::Cell<T>::setDrawStateOfUpperBorder(const bool value) noexcept
{
    this->_draw_upper_border = value;
}

template <typename T> void types::Cell<T>::setDrawStateOfLowerBorder(const bool value) noexcept
{
    this->_draw_lower_border = value;
}

template <typename T> void types::Cell<T>::setDrawStateOfLeftBorder(const bool value) noexcept
{
    this->_draw_left_border = value;
}

template <typename T> void types::Cell<T>::setDrawStateOfRightBorder(const bool value) noexcept
{
    this->_draw_right_border = value;
}

template <typename T> types::Cell<T>::Cell()
{
    this->_data = T();
    setDrawStateOfUpperBorder(true);
    setDrawStateOfLowerBorder(true);
    setDrawStateOfLeftBorder(true);
    setDrawStateOfRightBorder(true);

    setStateOfUpperBorder(true);
    setStateOfLowerBorder(true);
    setStateOfLeftBorder(true);
    setStateOfRightBorder(true);

    setUpperBorderChar(DEFAULT_HORIZONTAL_BORDER_CHAR);
    setLowerBorderChar(DEFAULT_HORIZONTAL_BORDER_CHAR);
    setRightBorderChar(DEFAULT_VERTICAL_BORDER_CHAR);
    setLeftBorderChar(DEFAULT_VERTICAL_BORDER_CHAR);

    setUpperLeftCornerChar(DEFAULT_CORNER_CHAR);
    setUpperRightCornerChar(DEFAULT_CORNER_CHAR);
    setLowerLeftCornerChar(DEFAULT_CORNER_CHAR);
    setLowerRightCornerChar(DEFAULT_CORNER_CHAR);

    this->_alignment = eAlignment::ALIGN_LEFT;
}

template <typename T> types::Cell<T>::~Cell<T>()
{
    // Empty on purpose
}

template <typename T> types::Cell<T>::Cell(T value)
{
    this->_data = value;
    setDrawStateOfUpperBorder(true);
    setDrawStateOfLowerBorder(true);
    setDrawStateOfLeftBorder(true);
    setDrawStateOfRightBorder(true);

    setStateOfUpperBorder(true);
    setStateOfLowerBorder(true);
    setStateOfLeftBorder(true);
    setStateOfRightBorder(true);

    setUpperBorderChar(DEFAULT_HORIZONTAL_BORDER_CHAR);
    setLowerBorderChar(DEFAULT_HORIZONTAL_BORDER_CHAR);
    setRightBorderChar(DEFAULT_VERTICAL_BORDER_CHAR);
    setLeftBorderChar(DEFAULT_VERTICAL_BORDER_CHAR);

    setUpperLeftCornerChar(DEFAULT_CORNER_CHAR);
    setUpperRightCornerChar(DEFAULT_CORNER_CHAR);
    setLowerLeftCornerChar(DEFAULT_CORNER_CHAR);
    setLowerRightCornerChar(DEFAULT_CORNER_CHAR);

    this->_alignment = eAlignment::ALIGN_LEFT;
}

template <typename T>
types::Cell<T>::Cell(T value, const bool upper_border, const bool lower_border, const bool left_border,
                     const bool right_border)
{
    this->_data = value;
    setDrawingOfBorders(upper_border, lower_border, left_border, right_border);
    this->_alignment = eAlignment::ALIGN_LEFT;
}

template <typename T> [[nodiscard]] const T &types::Cell<T>::data() const noexcept
{
    return this->_data;
}

template <typename T> void types::Cell<T>::data(const T data) noexcept
{
    this->_data = data;
}

template <typename T> types::Table<T>::Table(const size_t rows, const size_t columns)
{
    if (rows == 0 || columns == 0)
    {
        this->_rows = 1;
        this->_columns = 1;
    }
    else
    {
        this->_rows = rows;
        this->_columns = columns;
    }

    this->_data = new Cell<T> *[this->_rows]();
    for (size_t i = 0; i < this->_rows; i++)
    {
        this->_data[i] = new Cell<T>[this->_columns]();
    }

    this->_column_widths = new size_t[this->_columns];
    for (size_t i = 0; i < this->_columns; i++)
    {
        this->_column_widths[i] = DEFAULT_COLUMN_WIDTH;
    }
    this->_padding = DEFAULT_PADDING;
}

template <typename T> void types::Table<T>::deleteWidths() noexcept
{
    delete[] this->_column_widths;
}

template <typename T> types::Table<T>::~Table<T>()
{
    deleteData();
    deleteWidths();
}

template <typename T> [[nodiscard]] size_t types::Table<T>::rows() const noexcept
{
    return this->_rows;
}

template <typename T> [[nodiscard]] size_t types::Table<T>::columns() const noexcept
{
    return this->_columns;
}

template <typename T> void types::Table<T>::deleteData() noexcept
{
    for (size_t i = 0; i < this->_rows; i++)
        delete[] this->_data[i];
    delete[] this->_data;
}

template <typename T> void types::Table<T>::addRow()
{
    Cell<T> **newData = new Cell<T> *[this->_rows + 1];
    for (size_t i = 0; i < this->_rows + 1; i++)
    {
        newData[i] = new Cell<T>[this->_columns];
    }

    for (size_t i = 0; i < this->_rows; i++)
    {
        for (size_t j = 0; j < this->_columns; j++)
        {
            newData[i][j] = this->_data[i][j];
        }
    }

    for (size_t i = 0; i < this->_columns; i++)
    {
        newData[this->_rows - 1][i].setDrawStateOfLowerBorder(true);
        newData[this->_rows - 1][i].setDrawStateOfLowerBorder(true);
    }

    deleteData();

    this->_rows++;
    this->_data = newData;
}

template <typename T> void types::Table<T>::addColumn()
{
    Cell<T> **newData = new Cell<T> *[this->_rows];
    size_t *newWidths = new size_t[this->_columns + 1];
    for (size_t i = 0; i < this->_rows; i++)
    {
        newData[i] = new Cell<T>[this->_columns + 1];
    }

    for (size_t i = 0; i < this->_rows; i++)
    {
        for (size_t j = 0; j < this->_columns; j++)
        {
            newData[i][j] = this->_data[i][j];
        }
    }

    for (size_t i = 0; i < this->_columns; i++)
    {
        newWidths[i] = this->_column_widths[i];
    }
    newWidths[this->_columns] = DEFAULT_COLUMN_WIDTH;

    for (size_t i = 0; i < this->_rows; i++)
    {
        newData[i][this->_columns - 1].setDrawStateOfRightBorder(true);
        newData[i][this->_columns - 1].setDrawStateOfRightBorder(true);
    }

    deleteData();
    deleteWidths();

    this->_columns++;
    this->_data = newData;
    this->_column_widths = newWidths;
}
template <typename T>
[[nodiscard]] const types::Cell<T> &types::Table<T>::getCell(const size_t row, const size_t column) const
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_columns");
    return this->_data[row][column];
}

template <typename T> void types::Table<T>::setCell(const size_t row, const size_t column, const types::Cell<T> cell)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_columns");
    this->_data[row][column] = cell;
}

template <typename T> const T &types::Table<T>::getElement(const size_t row, const size_t column) const
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_columns");
    return this->_data[row][column].data();
}

template <typename T> void types::Table<T>::setElement(const size_t row, const size_t column, const T value)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_columns");
    this->_data[row][column].data(value);
}

template <typename T> [[nodiscard]] size_t types::Table<T>::padding() const noexcept
{
    return this->_padding;
}

template <typename T> void types::Table<T>::padding(const size_t value) noexcept
{
    this->_padding = value;
}

template <typename T> void types::Table<T>::automaticallySizeColumns() noexcept
{
    for (size_t col = 0; col < this->_columns; col++)
    {
        size_t biggest = 0;
        for (size_t row = 0; row < this->_rows; row++)
        {
            size_t tempsize = types::to_string(getElement(row, col)).size();
            size_t tempsize2 = getCell(row, col).getHeading().size();
            if (tempsize2 > tempsize)
                tempsize = tempsize2;
            if (tempsize > biggest)
                biggest = tempsize;
        }
        this->_column_widths[col] = biggest;
    }
}

template <typename T> void types::Table<T>::setColumnHeading(const size_t column, const string title)
{
    if (column >= this->_columns)
        throw std::out_of_range("column is bigger than this->_columns");
    this->_data[0][column].setHeading(title);
}

template <typename T> string types::Table<T>::getColumnHeading(const size_t column) const
{
    if (column >= this->_columns)
        throw std::out_of_range("column is bigger than this");
    return this->_data[0][column].getHeading();
}

template <typename T> void types::Table<T>::drawTable() const noexcept
{
#ifndef _WIN32
    const char* const localeName = "en_US.utf-8";
    std::locale::global(std::locale(localeName));
#endif
    PRINTLN(generateTableAsString(false));
}

template <typename T>
[[nodiscard]] string types::Table<T>::getAlignedString(const string content, const size_t target_width,
                                                            const types::eAlignment alignment,
                                                            const character filler) const noexcept
{
    size_t leftspace = 0;
    size_t rightspace = 0;
    string output = EMPTY_STRING;

    if (content.size() == target_width)
        return content;
    if(content.size() > target_width)
    {
        switch(alignment)
        {
        case types::eAlignment::ALIGN_RIGHT:
            {
                for(size_t i = 0; i < target_width; i++)
                {
                    output += content[content.size() - target_width + i];
                }
                return output;
            } break;
        default:
            for(size_t i = 0; i < target_width; i++)
            {
                output += content[i];
            }
            return output;
        }
    }

    switch (alignment)
    {
    case types::eAlignment::ALIGN_LEFT: {
        output = content;
        while (output.size() != target_width)
        {
            output += filler;
        }
        return output;
    }
    break;
    case types::eAlignment::ALIGN_CENTER: {
        leftspace = (target_width - content.size()) / 2;
        rightspace = leftspace;

        if (leftspace + rightspace < target_width - content.size())
            rightspace++;

        for (size_t i = 0; i < leftspace; i++)
        {
            output += filler;
        }
        output += content;
        for (size_t i = 0; i < rightspace; i++)
        {
            output += filler;
        }
        return output;
    }
    break;
    case types::eAlignment::ALIGN_RIGHT: {
        output = content;
        while (output.size() != target_width)
        {
            output = filler + output;
        }
        return output;
    }
    break;
    }
    return content;
}

template <typename T>
[[nodiscard]] string types::Table<T>::generateTableAsString(const bool newline) const noexcept
{
    string table;

    for (size_t row = 0; row < this->_rows; row++)
    {
        // if(row == 0)
        {
            bool drawLine = false;
            for (size_t col = 0; col < this->_columns; col++)
            {
                if (this->_data[row][col].getStateOfUpperBorder() == true)
                {
                    drawLine = true;
                    break;
                }
            }
            if (drawLine)
            {
                for (size_t col = 0; col < this->_columns; col++)
                {
                    corner_s corner = {};
                    const types::Cell<T> &curr = this->_data[row][col];
                    if (curr.getStateOfLeftBorder() && curr.getDrawStateOfLeftBorder())
                        corner.a = true;
                    if (curr.getStateOfUpperBorder() && curr.getDrawStateOfUpperBorder())
                        corner.b = true;
                    if (col > 0 && (this->_data[row][col - 1].getStateOfUpperBorder() &&
                                    this->_data[row][col - 1].getDrawStateOfUpperBorder()))
                        corner.c = true;
                    if (row > 0 && (this->_data[row - 1][col].getStateOfLeftBorder() &&
                                    this->_data[row - 1][col].getDrawStateOfLeftBorder()))
                        corner.d = true;

                    switch (corner.a + corner.b + corner.c + corner.d)
                    {
                    case 1: {
                        table += ' ';
                    }
                    break;
                    case 2: {
                        if (corner.b && corner.c)
                        {
                            table += curr.getUpperBorderChar();
                        }
                        else if (corner.a && corner.d)
                        {
                            table += curr.getLeftBorderChar();
                        }
                        else
                        {
                            table += curr.getUpperLeftCornerChar();
                        }
                    }
                    break;
                    default: {
                        table += curr.getUpperLeftCornerChar();
                    }
                    break;
                    }

                    if (corner.b)
                        table += getAlignedString(curr.getHeading(), this->_column_widths[col] + 2 * this->_padding,
                                                  types::eAlignment::ALIGN_CENTER, curr.getUpperBorderChar());
                    else
                        table += getAlignedString(EMPTY_STRING, this->_column_widths[col] + 2 * this->_padding,
                                                  types::eAlignment::ALIGN_LEFT, SPACE_CHAR);
                }
                corner_s corner = {};

                if (this->_data[row][this->_columns - 1].getDrawStateOfUpperBorder() &&
                    this->_data[row][this->_columns - 1].getStateOfUpperBorder())
                    corner.a = true;
                if (this->_data[row][this->_columns - 1].getDrawStateOfRightBorder() &&
                    this->_data[row][this->_columns - 1].getStateOfRightBorder())
                    corner.b = true;
                if (row > 0 && (this->_data[row - 1][this->_columns - 1].getDrawStateOfRightBorder() &&
                                this->_data[row - 1][this->_columns - 1].getStateOfRightBorder()))
                    corner.c = true;

                switch (corner.a + corner.b + corner.c)
                {
                case 3: {
                    table += this->_data[row][this->_columns - 1].getUpperRightCornerChar();
                }
                break;
                case 2: {
                    if (corner.b && corner.c)
                        table += this->_data[row][this->_columns - 1].getRightBorderChar();
                    else
                        table += this->_data[row][this->_columns - 1].getUpperRightCornerChar();
                }
                break;
                default:
                    table += ' ';
                }
                table += '\n';
            }
        }

        {
            // Drawing the contents of cells
            for (size_t col = 0; col < this->_columns; col++)
            {
                const types::Cell<T> &curr = this->_data[row][col];

                if (col == 0)
                {
                    if (curr.getDrawStateOfLeftBorder() && curr.getStateOfLeftBorder())
                        table += curr.getLeftBorderChar(); // border
                    else
                        table += ' ';
                }

                for (size_t i = 0; i < this->_padding; i++)
                    table += ' '; // padding

                table += getAlignedString(types::to_string(curr.data()), this->_column_widths[col], curr.getAlignment(),
                                          ' '); // content of cell

                for (size_t i = 0; i < this->_padding; i++)
                    table += ' '; // padding

                if (curr.getDrawStateOfRightBorder() && curr.getStateOfRightBorder())
                    table += curr.getRightBorderChar(); // border
                else
                    table += ' ';
            }
            table += '\n';
        }

        if (row == this->_rows - 1)
        {
            bool drawLine = false;
            for (size_t col = 0; col < this->_columns; col++)
            {
                if (this->_data[row][col].getStateOfLowerBorder() == true)
                {
                    drawLine = true;
                    break;
                }
            }
            if (drawLine)
            {
                for (size_t col = 0; col < this->_columns; col++)
                {
                    corner_s corner = {};
                    const types::Cell<T> &ref = this->_data[this->_rows - 1][col];
                    if (ref.getStateOfLowerBorder() && ref.getDrawStateOfLowerBorder())
                        corner.a = true;
                    if (ref.getStateOfLeftBorder() && ref.getDrawStateOfLeftBorder())
                        corner.b = true;
                    if (col > 0 && (this->_data[this->_rows - 1][col - 1].getDrawStateOfLowerBorder() &&
                                    this->_data[this->_rows - 1][col - 1].getStateOfLowerBorder()))
                        corner.c = true;

                    switch (corner.a + corner.b + corner.c)
                    {
                    case 3: {
                        table += ref.getLowerLeftCornerChar();
                    }
                    break;
                    case 2: {
                        if (corner.a && corner.c)
                            table += ref.getLowerBorderChar();
                        else
                            table += ref.getLowerLeftCornerChar();
                    }
                    break;
                    default:
                        table += ' ';
                    }

                    if (corner.a)
                        table += getAlignedString(EMPTY_STRING, this->_column_widths[col] + 2 * this->_padding,
                                                  types::eAlignment::ALIGN_LEFT, ref.getLowerBorderChar());
                    else
                        table += getAlignedString(EMPTY_STRING, this->_column_widths[col] + 2 * this->_padding,
                                                  types::eAlignment::ALIGN_LEFT, SPACE_CHAR);
                }

                corner_s corner = {};
                const types::Cell<T> &ref = this->_data[this->_rows - 1][this->_columns - 1];
                if (ref.getStateOfLowerBorder() && ref.getDrawStateOfLowerBorder())
                    corner.a = true;
                if (ref.getStateOfRightBorder() && ref.getDrawStateOfRightBorder())
                    corner.b = true;

                switch (corner.a + corner.b)
                {
                case 2: {
                    table += ref.getLowerRightCornerChar();
                }
                break;
                default:
                    table += ' ';
                }
            }
        }
    }

    if (newline && table[table.size() - 1] != '\n')
        table += '\n';
    else
    {
        if (table[table.size() - 1] == '\n')
            table = table.substr(0, table.size() - 1);
    }
    return table;
}

template <typename T> void types::Table<T>::exportTableAsCSV(const std::filesystem::path path, const char div) const
{
    std::ofstream file(path.string());
    if(!file.is_open()) throw std::ofstream::failure("Exception opening file.");
    
    std::string output = "";
    for(size_t row = 0; row < this->_rows; row++)
    {
        for(size_t col = 0; col < this->_columns; col++)
        {
            output += types::to_string(this->_data[row][col].data());
            if(col + 1 < this->_columns) output += div;
        }
        output += '\n';
    }
    
    file.write(output.c_str(), output.size());
    file.close();
}

template <typename T> void types::Table<T>::importTableFromCSV(const std::filesystem::path path, const char div)
{
    std::ifstream file;
    file.open(path, std::ios::in);
    if(!file.is_open()) throw std::ifstream::failure("Exception opening file.");

    size_t length;
    file.seekg(0, std::ios::end);
    length = file.tellg();
    file.seekg(0, std::ios::beg);
    
    char* buf = new char[length];
    
    file.read(buf, length);
    file.close(); 

    size_t row = 0, col = 0;
    std::string temp = "";
    for(size_t i = 0; i < length; i++)
    {
        char c = buf[i];
        if(c == div || c == '\n'){
            if(col == this->_columns) addColumn();
            setElement(row, col, to_type(temp));
            if(c == '\n' && buf[i + 1] != '\0') {
                addRow();
                row++;
                col = 0;
            } else col++;
            temp = "";
        }
        else temp += c;
    }
    delete[] buf;
}

template <typename T> [[nodiscard]] size_t types::Table<T>::getColumnWidth(const size_t column) const
{
    if (column >= this->_columns)
        throw std::out_of_range("column is bigger than this->_columns");
    return this->_column_widths[column];
}

template <typename T> void types::Table<T>::setColumnWidth(const size_t column, const size_t width)
{
    if (column >= this->_columns)
        throw std::out_of_range("column is bigger than this->columns");
    this->_column_widths[column] = width;
}

template <typename T> void types::Table<T>::enableDrawingCellBorders() noexcept
{
    for (size_t row = 0; row < this->_rows; row++)
    {
        for (size_t col = 0; col < this->_columns; col++)
        {
            this->_data[row][col].setDrawingOfBorders(true, true, true, true);
        }
    }
    this->_CellBorderDrawingState = true;
}

template <typename T> void types::Table<T>::disableDrawingCellBorders() noexcept
{
    for (size_t row = 0; row < this->_rows; row++)
    {
        for (size_t col = 0; col < this->_columns; col++)
        {
            this->_data[row][col].setDrawingOfBorders(false, false, false, false);
        }
    }
    this->_CellBorderDrawingState = false;
}

template <typename T> void types::Table<T>::setCellBorderDrawingState(const bool state) const noexcept
{
    for (size_t row = 0; row < this->_rows; row++)
    {
        for (size_t col = 0; col < this->_columns; col++)
        {
            this->_data[row][col].setDrawingOfBorders(state, state, state, state);
        }
    }
    this->_CellBorderDrawingState = state;
}

template <typename T> bool types::Table<T>::getCellBorderDrawingState() const noexcept
{
    return this->_CellBorderDrawingState;
}

template <typename T> void types::Table<T>::enableDrawingTableBorders() noexcept
{
    for (size_t col = 0; col < this->_columns; col++)
    {
        this->_data[0][col].setDrawStateOfUpperBorder(true);
        this->_data[this->_rows - 1][col].setDrawStateOfLowerBorder(true);
    }
    for (size_t row = 0; row < this->_rows; row++)
    {
        this->_data[row][0].setDrawStateOfLeftBorder(true);
        this->_data[row][this->_columns - 1].setDrawStateOfRightBorder(true);
    }
    this->_TableBordersDrawingState = true;
}

template <typename T> void types::Table<T>::disableDrawingTableBorders() noexcept
{
    for (size_t col = 0; col < this->_columns; col++)
    {
        this->_data[0][col].setDrawStateOfUpperBorder(false);
        this->_data[this->_rows - 1][col].setDrawStateOfLowerBorder(false);
    }
    for (size_t row = 0; row < this->_rows; row++)
    {
        this->_data[row][0].setDrawStateOfLeftBorder(false);
        this->_data[row][this->_columns - 1].setDrawStateOfRightBorder(false);
    }
    this->_TableBordersDrawingState = false;
}

template <typename T> [[nodiscard]] bool types::Table<T>::getTableBorderDrawingState() const noexcept
{
    return this->_TableBordersDrawingState;
}

template <typename T> void types::Table<T>::setTableBorderDrawingState(const bool state) noexcept
{
    for (size_t col = 0; col < this->_columns; col++)
    {
        this->_data[0][col].setDrawStateOfUpperBorder(state);
        this->_data[this->_rows - 1][col].setDrawStateOfLowerBorder(state);
    }
    for (size_t row = 0; row < this->_rows; row++)
    {
        this->_data[row][0].setDrawStateOfLeftBorder(state);
        this->_data[row][this->_columns - 1].setDrawStateOfRightBorder(state);
    }
    this->_TableBordersDrawingState = state;
}

template <typename T> types::eAlignment types::Table<T>::getAlignmentOfCell(const size_t row, const size_t column) const
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    return getCell(row, column).getAlignment();
}

template <typename T>
void types::Table<T>::setAlignmentOfCell(const size_t row, const size_t column, const types::eAlignment alignment)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    this->_data[row][column].setAlignment(alignment);
}

template <typename T>
void types::Table<T>::setDrawingUpperBorderOfCell(const size_t row, const size_t column, const bool state)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    this->_data[row][column].setDrawStateOfUpperBorder(state);
    if (row > 0)
        this->_data[row - 1][column].setDrawStateOfLowerBorder(state);
}

template <typename T>
void types::Table<T>::setDrawingLowerBorderOfCell(const size_t row, const size_t column, const bool state)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    this->_data[row][column].setDrawStateOfLowerBorder(state);
    if (row < this->_rows - 1)
        this->_data[row + 1][column].setDrawStateOfUpperBorder(state);
}

template <typename T>
void types::Table<T>::setDrawingLeftBorderOfCell(const size_t row, const size_t column, const bool state)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    this->_data[row][column].setDrawStateOfLeftBorder(state);
    if (column > 0)
        this->_data[row][column - 1].setDrawStateOfRightBorder(state);
}

template <typename T>
void types::Table<T>::setDrawingRightBorderOfCell(const size_t row, const size_t column, const bool state)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    this->_data[row][column].setDrawStateOfRightBorder(state);
    if (column < this->_columns - 1)
        this->_data[row][column + 1].setDrawStateOfLeftBorder(state);
}

template <typename T>
[[nodiscard]] string types::Table<T>::getHeadingOfCell(const size_t row, const size_t column) const
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    return this->_data[row][column].getHeading();
}

template <typename T>
void types::Table<T>::setHeadingOfCell(const size_t row, const size_t column, const string value)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    this->_data[row][column].setHeading(value);
}

template <typename T> [[nodiscard]] string types::Cell<T>::getHeading() const noexcept
{
    return this->_heading;
}

template <typename T> void types::Cell<T>::setHeading(const string heading) noexcept
{
    this->_heading = heading;
}

template <typename T> [[nodiscard]] bool types::Cell<T>::getStateOfUpperBorder() const noexcept
{
    return this->_upper_border;
}

template <typename T> [[nodiscard]] bool types::Cell<T>::getStateOfLowerBorder() const noexcept
{
    return this->_lower_border;
}

template <typename T> [[nodiscard]] bool types::Cell<T>::getStateOfLeftBorder() const noexcept
{
    return this->_left_border;
}

template <typename T> [[nodiscard]] bool types::Cell<T>::getStateOfRightBorder() const noexcept
{
    return this->_right_border;
}
template <typename T> void types::Cell<T>::setStateOfUpperBorder(const bool value) noexcept
{
    this->_upper_border = value;
}

template <typename T> void types::Cell<T>::setStateOfLowerBorder(const bool value) noexcept
{
    this->_lower_border = value;
}

template <typename T> void types::Cell<T>::setStateOfLeftBorder(const bool value) noexcept
{
    this->_left_border = value;
}

template <typename T> void types::Cell<T>::setStateOfRightBorder(const bool value) noexcept
{
    this->_right_border = value;
}

template <typename T>
void types::Table<T>::setUpperBorderOfCell(const size_t row, const size_t column, const bool state)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    this->_data[row][column].setStateOfUpperBorder(state);
    if (row > 0)
        this->_data[row - 1][column].setStateOfLowerBorder(state);
}

template <typename T>
void types::Table<T>::setLowerBorderOfCell(const size_t row, const size_t column, const bool state)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    this->_data[row][column].setStateOfLowerBorder(state);
    if (row + 1 < this->_rows)
        this->_data[row + 1][column].setStateOfUpperBorder(state);
}

template <typename T> void types::Table<T>::setLeftBorderOfCell(const size_t row, const size_t column, const bool state)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    this->_data[row][column].setStateOfLeftBorder(state);
    if (column > 0)
        this->_data[row][column - 1].setStateOfRightBorder(state);
}

template <typename T>
void types::Table<T>::setRightBorderOfCell(const size_t row, const size_t column, const bool state)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    this->_data[row][column].setStateOfRightBorder(state);
    if (column + 1 < this->_columns)
        this->_data[row][column + 1].setStateOfLeftBorder(state);
}

template <typename T>
void types::Table<T>::setUpperBorderCharOfCell(const size_t row, const size_t column, const character c)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    this->_data[row][column].setUpperBorderChar(c);
    if (row > 0)
        this->_data[row - 1][column].setLowerBorderChar(c);
}

template <typename T>
void types::Table<T>::setLowerBorderCharOfCell(const size_t row, const size_t column, const character c)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    this->_data[row][column].setLowerBorderChar(c);
    if (row + 1 < this->_rows)
        this->_data[row + 1][column].setUpperBorderChar(c);
}

template <typename T>
void types::Table<T>::setLeftBorderCharOfCell(const size_t row, const size_t column, const character c)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    this->_data[row][column].setLeftBorderChar(c);
    if (column > 0)
        this->_data[row][column - 1].setRightBorderChar(c);
}

template <typename T>
void types::Table<T>::setRightBorderCharOfCell(const size_t row, const size_t column, const character c)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    this->_data[row][column].setRightBorderChar(c);
    if (column + 1 < this->_columns)
        this->_data[row][column + 1].setLeftBorderChar(c);
}

template <typename T>
void types::Table<T>::setUpperLeftCornerCharOfCell(const size_t row, const size_t column, const character c)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    this->_data[row][column].setUpperLeftCornerChar(c);
    if (row > 0)
        this->_data[row - 1][column].setLowerLeftCornerChar(c);
    if (column > 0)
        this->_data[row][column - 1].setUpperRightCornerChar(c);
    if (row > 0 && column > 0)
        this->_data[row - 1][column - 1].setLowerRightCornerChar(c);
}

template <typename T>
void types::Table<T>::setUpperRightCornerCharOfCell(const size_t row, const size_t column, const character c)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    this->_data[row][column].setUpperRightCornerChar(c);
    if (column + 1 < this->_columns)
        this->_data[row][column + 1].setUpperLeftCornerChar(c);
    if (row > 0)
        this->_data[row - 1][column].setLowerRightCornerChar(c);
    if (row > 0 && column + 1 < this->_columns)
        this->_data[row - 1][column + 1].setLowerLeftCornerChar(c);
}

template <typename T>
void types::Table<T>::setLowerLeftCornerCharOfCell(const size_t row, const size_t column, const character c)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    this->_data[row][column].setLowerLeftCornerChar(c);
    if (column > 0)
        this->_data[row][column - 1].setLowerRightCornerChar(c);
    if (row + 1 < this->_rows)
        this->_data[row + 1][column].setUpperLeftCornerChar(c);
    if (column > 0 && row + 1 < this->_rows)
        this->_data[row + 1][column - 1].setUpperRightCornerChar(c);
}

template <typename T>
void types::Table<T>::setLowerRightCornerCharOfCell(const size_t row, const size_t column, const character c)
{
    if (row >= this->_rows || column >= this->_columns)
        throw std::out_of_range("row or column is bigger than this->_rows or this->_column");
    this->_data[row][column].setLowerRightCornerChar(c);
    if (column + 1 < this->_columns)
        this->_data[row][column + 1].setLowerRightCornerChar(c);
    if (row + 1 < this->_rows)
        this->_data[row + 1][column].setUpperRightCornerChar(c);
    if ((column + 1 < this->_columns) && (row + 1 < this->_rows))
        this->_data[row + 1][column + 1].setUpperLeftCornerChar(c);
}

template<typename T> void types::Table<T>::removeColumn(const size_t column)
{
    if (column >= this->_columns)
        throw std::out_of_range("column is bigger than this->_column");
    
    types::Cell<T>** newData = new Cell<T>*[this->_rows];
    size_t* newWidths = new size_t[this->_columns - 1];
    
    for(size_t row = 0; row < this->_rows; row++)
    {
        newData[row] = new Cell<T>[this->_columns - 1];
    }
    
    for(size_t row = 0; row < this->_rows; row++)
    {
        size_t nCol = 0;
        for(size_t col = 0; col < this->_columns; col++)
        {
            if(col != column)
            {
                newData[row][nCol] = this->_data[row][col];
                nCol++;
            } 
        }
    }

    size_t nCol = 0;
    for(size_t col = 0; col < this->_columns; col++)
    {
        if(col != column)
        {
            newWidths[nCol] = this->_column_widths[col];
            nCol++;
        }
    }

    deleteData();
    deleteWidths();

    this->_columns--;
    this->_data = newData;
    this->_column_widths = newWidths;
}
 
template<typename T> void types::Table<T>::removeRow(const size_t row)
{
    if (row >= this->_rows)
        throw std::out_of_range("row is bigger than this->_rows");
    
    types::Cell<T>** newData = new Cell<T>*[this->_rows - 1];
    
    for(size_t i = 0; i < this->_rows - 1; i++)
    {
        newData[i] = new Cell<T>[this->_columns];
    }

    for(size_t col = 0; col < this->_columns; col++)
    {
        size_t nRow = 0;
        for(size_t r = 0; r < this->_rows; r++)
        {
            if(r != row)
            {
                newData[nRow][col] = this->_data[r][col];
                nRow++;
            }
        }
    }

    deleteData();

    this->_rows--;
    this->_data = newData;
}
 
template<typename T> void types::Table<T>::insertCol(const size_t column)
{
    if (column >= this->_columns)
        throw std::out_of_range("column is bigger than this->_column");
    
    types::Cell<T>** newData = new Cell<T>*[this->_rows];
    size_t* newWidths = new size_t[this->_columns + 1];
    for(size_t row = 0; row < this->_rows; row++)
    {
        newData[row] = new Cell<T>[this->_columns + 1];
    }

    // Settings the borders of the columns in which the new one is inserted to true, as to not break the rendering.
    for(size_t row = 0; row < this->_rows; row++)
    {
        if(column > 0) this->_data[row][column- 1].setStateOfRightBorder(true);
        if(column + 1 < this->_columns) this->_data[row][column+ 1].setStateOfLeftBorder(true);
    }

    for(size_t row = 0; row < this->_rows; row++)
    {
        size_t oCol = 0;
        for(size_t col = 0; col < this->_columns + 1; col++)
        {
            if(col == column)
            {
                newData[row][col] = Cell<T>();
            } else 
            {
                newData[row][col] = this->_data[row][oCol];
                oCol++;    
            }
        }
    }

    size_t oCol = 0;
    for(size_t i = 0; i < this->_columns + 1; i++)
    {
        if(i == column)
        {
            newWidths[i] = DEFAULT_COLUMN_WIDTH;
        } else newWidths[i] = this->_column_widths[oCol++];
    }

    deleteData();
    deleteWidths();

    this->_column_widths = newWidths;
    this->_data = newData;
    this->_columns++;
}
 
template<typename T> void types::Table<T>::insertRow(const size_t row)
{
    if (row >= this->_rows)
        throw std::out_of_range("row is bigger than this->_rows");
    
    types::Cell<T>** newData = new Cell<T>*[this->_rows + 1];
    for(size_t i = 0; i < this->_rows + 1; i++)
    {
        newData[i] = new Cell<T>[this->_columns];
    }
    for(size_t col = 0; col < this->_columns; col++)
    {
        if(row > 0) this->_data[row - 1][col].setStateOfLowerBorder(true);
        if(row + 1 < this->_rows) this->_data[row + 1][col].setStateOfUpperBorder(true);
    }

    for(size_t col = 0; col < this->_columns; col++)
    {
        size_t oRow = 0;
        for(size_t r = 0; r < this->_rows + 1; r++)
        {
            if(r == row)
            {
                newData[r][col] = Cell<T>();
            } 
            else 
            {
                newData[r][col] = this->_data[oRow][col];
            }
        }
    }

    deleteData();

    this->_data = newData;
    this->_rows++;
}
 
