#pragma once

#include "./defines.hpp"

#define WCHAR_TABLE
#include "table.hpp"
#include <vector>
#include <string>
#include <filesystem>
#include <iostream>
#include <fstream>

namespace line
{
    [[nodiscard]] lines_t sumLines(const std::vector<lines_t> &lines) noexcept;
    void printTable(const std::vector<lines_t> &lines) noexcept;
    [[nodiscard]] lines_t findLines(const std::filesystem::directory_entry& p) noexcept;
}
namespace util {
    [[nodiscard]] bool isWhitespace(const char c) noexcept;
}
