#pragma once

#include "./defines.hpp"

#include <filesystem>
#include <vector>
#include <fstream>

namespace lineIO
{
    void exportCSV(const std::vector<lines_t>& lines, const std::filesystem::path path);
}
