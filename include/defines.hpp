#pragma once

#include <string>
#include <filesystem>

#define PATHWIDTH 35
#define LINECOUNTWIDTH 17
#define CSV_SEPERATOR ';'

typedef struct 
{
    std::filesystem::path p;
    int raw; // Raw line
    int bl;  // blank lines
    int wbl; // without blank lines
    int words; // Counts the number of unique words in a file
    std::string language; // Stores the language the file is writen in
} lines_t;
