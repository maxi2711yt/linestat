#include "../include/lineIO.hpp"

#ifdef _WIN32
typedef std::wstring string;
typedef std::wofstream outputstream;
#define EMPTY_STRING L""
#else
typedef std::string string;
typedef std::ofstream outputstream;
#define EMPTY_STRING ""
#endif

namespace overload
{
    template<typename T> [[nodiscard]] string to_string(const T value)
    {
#ifdef _WIN32
        return std::to_wstring(value);
#else
        return std::to_string(value);
#endif
    }
}

string getFormatedCSVString(const lines_t& line, bool newline)
{
    string out = EMPTY_STRING;
    out += line.p.c_str();
    out += CSV_SEPERATOR;
    out += overload::to_string(line.raw);
    out += CSV_SEPERATOR;
    out += overload::to_string(line.bl);
    out += CSV_SEPERATOR;
    out += overload::to_string(line.wbl);
    if(newline) out += '\n';
    return out;
}

void exportToFile(const string& content, const std::filesystem::path& path)
{
    outputstream file(path.string());
    if(!file.is_open())
    {
        return;
    }

    file.write(content.c_str(), content.size());
    file.close();
}

void lineIO::exportCSV(const std::vector<lines_t>& lines, const std::filesystem::path path)
{
    string output = EMPTY_STRING;
    for(const lines_t& line : lines)
    {
        output += getFormatedCSVString(line, true);
    }
    exportToFile(output, path);
}
