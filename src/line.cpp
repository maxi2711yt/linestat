#include "../include/line.hpp"
#include "../include/defines.hpp"

[[nodiscard]] lines_t line::sumLines(const std::vector<lines_t>& lines) noexcept
{
    lines_t sum = {};
    sum.p = std::filesystem::path("sum");
    for (const lines_t &line : lines)
    {
        sum.raw += line.raw;
        sum.wbl += line.wbl;
        sum.bl += line.bl;
    }
    return sum;
}

void line::printTable(const std::vector<lines_t> &lines) noexcept
{
    types::Table<string> table(lines.size(), 4);
    table.setColumnHeading(0, L"Name");
    table.setColumnHeading(1, L"Lines");
    table.setColumnHeading(2, L"Lines NBL");
    table.setColumnHeading(3, L"Blank Lines");

    // table.disableDrawingTableBorders();

    size_t row = 0;
    for(const lines_t& line : lines )
    {
        table.setElement(row, 0, types::to_string(line.p.string()));
        table.setLowerBorderOfCell(row, 0, false);

        table.setElement(row, 1, types::to_string(line.raw));
        table.setAlignmentOfCell(row, 1, types::eAlignment::ALIGN_RIGHT);
        table.setLowerBorderOfCell(row, 1, false);
        
        table.setElement(row, 2, types::to_string(line.wbl));
        table.setAlignmentOfCell(row, 2, types::eAlignment::ALIGN_RIGHT);
        table.setLowerBorderOfCell(row, 2, false);
        
        table.setElement(row, 3, types::to_string(line.bl));
        table.setAlignmentOfCell(row, 3, types::eAlignment::ALIGN_RIGHT);
        table.setLowerBorderOfCell(row, 3, false);
    
        row++;
    }

    lines_t sum = sumLines(lines); 

    table.addRow();
    table.setElement(row, 0, types::to_string(sum.p.string()));
    table.setElement(row, 1, types::to_string(sum.raw));
    table.setAlignmentOfCell(row, 1, types::eAlignment::ALIGN_RIGHT);
    table.setElement(row, 2, types::to_string(sum.wbl));
    table.setAlignmentOfCell(row, 2, types::eAlignment::ALIGN_RIGHT);
    table.setElement(row, 3, types::to_string(sum.bl));
    table.setAlignmentOfCell(row, 3, types::eAlignment::ALIGN_RIGHT);

    table.automaticallySizeColumns();
    if(table.getColumnWidth(0) > 30) table.setColumnWidth(0, 30);
    table.drawTable();
}

[[nodiscard]] inline std::string formatNumberString(const int in, const size_t length) noexcept
{
    std::string output = std::to_string(in);
    if(output.size() >= length) return "";
    if(output.size() < length)
    {
        const int size = output.size();
        for(int i = length - size; i > 0; i--) output = " " + output;
    }
    return output;
}

[[nodiscard]] inline std::string formatPathString(const std::string& in, const size_t length) noexcept
{
    std::string output = in;
    if (output.size() > length)
    {
        output = "..." + output.substr(output.size() - length, output.size() - 1);
    }
    else
    {
        const int size = output.size();
        for (int i = length + 3 - size; i > 0; i--)
        {
            output += " ";
        }
    }
    return output;
}

[[nodiscard]] bool util::isWhitespace(const char c) noexcept
{
    switch (c)
    {
    case ' ':
        return true;
    case '\t':
        return true;
    case '\r':
        return true;
    case '\v':
        return true;
    case '\f':
        return true;
    }
    return false;
}

[[nodiscard]] lines_t line::findLines(const std::filesystem::directory_entry& p) noexcept
{
    lines_t output = {};
    output.p = p.path();

    if (!p.is_regular_file())
    {
        return output;
    }

    std::ifstream f(p.path().c_str());
    if (!f.is_open())
        return output;

    f.seekg(0, f.end);
    int length = f.tellg();
    f.seekg(0, f.beg);

    char *buf = new char[length];

    f.read(buf, length);

    f.close();

    for (int i = 0; i < length; i++)
    {
        if (buf[i] == '\n')
        {
            if (i > 0)
            {
                bool emptyline = true;
                int b;
                for (b = i - 1; b >= 0 && buf[b] != '\n' && emptyline; b--)
                {
                    if (!util::isWhitespace(buf[b]))
                    {
                        emptyline = false;
                    }
                }
                if (emptyline)
                    output.bl++;
            }
            output.raw++;
        }
    }
    output.wbl = output.raw - output.bl;
    delete[] buf;
    return output;
}
