#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "../include/defines.hpp"
#include "../include/line.hpp"
#include "../include/lineIO.hpp"

int main(int argc, char **argv)
{
    std::string path = ".";
    std::vector<lines_t> lines = {};
    int option = 0;
    int start = 1;
    std::string outputpath;

    if(argc > 1 && (std::string)argv[1] == "--export-csv")
    {
        if(argc < 3)
        {
            std::cout << "Not enough arguments provided!" << std::endl;
            return 1;
        }
        outputpath = (std::string)argv[2];
        start = 3;
        option = 1;
        
        if(argc == 3)
        {
            argv[2] = (char*)".";
            start = 2;
        }
    }

    if (argc == 1)
    {
        for (const std::filesystem::directory_entry &dir_entry : std::filesystem::recursive_directory_iterator(path))
        {
            lines.push_back(line::findLines(dir_entry));
        }
    }
    else
    {
        for (int i = start; i < argc; i++)
        {
            std::filesystem::path p(argv[i]);
            if (std::filesystem::is_directory(p) == true)
            {
                for (const std::filesystem::directory_entry &dir_entry :
                     std::filesystem::recursive_directory_iterator(p))
                {
                    lines.push_back(line::findLines(dir_entry));
                }
            }
            else
            {
                lines.push_back(line::findLines(std::filesystem::directory_entry(p)));
            }
        }
    }

    if(option == 1) lineIO::exportCSV(lines, outputpath);
    line::printTable(lines);
    return 0;
}
